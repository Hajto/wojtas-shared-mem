//
// Created by Haito on 20.12.2017.
//

#include "shared_memory.h"
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define KEYGEN_CONSTANT 123
#define INITIAL_MEMORY_START NULL

int SharedMemory::gen_id(int key) {
    int pamiec=shmget(key, bytes_of_memory, 0600|IPC_CREAT);
    if (pamiec==-1){
       throw SharedMemoryCompanion::SharedMemoryException("Could not create ");
    }
    else printf("Pamiec dzielona zostala utworzona : %d\n",pamiec);

    this->memid = pamiec;
    return pamiec;
}

void SharedMemory::obtain_address(int id) {
    void* adres=shmat(id, INITIAL_MEMORY_START, 0);
    if ((int) (intptr_t) adres == -1) throw SharedMemoryCompanion::SharedMemoryException("Could not access shared memory");
    else printf("Przestrzen adresowa zostala przyznana\n");

    this->shared_address = adres;
}

SharedMemory::SharedMemory(std::string key_root, int bytes_of_mem): bytes_of_memory(bytes_of_mem){
    int key = ftok(key_root.c_str(), KEYGEN_CONSTANT);
    memid = gen_id(key);
    obtain_address(memid);
}

SharedMemory::~SharedMemory() {
    int ctl_status = shmctl(memid,IPC_RMID,0);
    int dt_status = shmdt(shared_address);
    if (ctl_status == -1 ||  dt_status == -1) {
        throw SharedMemoryCompanion::SharedMemoryException("Could not detach memory properly");
    }
    else printf("Pamiec dzielona zostala odlaczona.\n");
}

void SharedMemory::access_shared_memory(const std::function<void(void *)> &lend) {
    if(shared_address != NULL) {
        lend(shared_address);
    }
}
