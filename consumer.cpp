#include <iostream>
#include "semaphore_control.h"
#include "shared_memory.h"
#include <fstream>

char consume(int semid, SharedMemory* mem);

int main(int argc, char** args) {
    int semid = Semaphore::get_shared_semaphore(2);
    SharedMemory* memory = new SharedMemory("/", 1);

    //Consume
    std::ofstream off("wynik.txt");
    if(!off.good()) return 1;
    char buffer = 0;
    while(buffer = consume(semid, memory), buffer != EOF) {
        off<<buffer;
    }
    off.close();

    Semaphore::clear_sempahores(semid);
    return 0;
}

char consume(int semid, SharedMemory *mem) {
    char buffer = 0;
    Semaphore::semaphore_await(semid,1);§
    mem->access_shared_memory([&buffer](void* data){
        buffer = *((char*) data);
    });
    Semaphore::semaphore_unlock(semid, 0);
    return buffer;
}


