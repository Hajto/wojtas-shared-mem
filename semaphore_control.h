//
// Created by Haito on 06.12.2017.
//

#ifndef WOJTAS_SEMAFORY_SEMAPHORE_CONTROL_H
#define WOJTAS_SEMAFORY_SEMAPHORE_CONTROL_H

#include <string>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

namespace Semaphore {
    std::string get_path();

    int get_shared_semaphore(int count);

    int init_semaphore(int count);

    int create_semaphore(key_t sem_key, int how_many);

    int get_created_semaphore(key_t sem_key, int how_many);

    void reset_semaphore(int semid);

    void semaphore_await(int sem_cluster_id, unsigned short which);

    void semaphore_unlock(int semid, unsigned short which);

    void clear_sempahores(int semid);
}

#endif //WOJTAS_SEMAFORY_SEMAPHORE_CONTROL_H
