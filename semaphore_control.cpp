#include <cstdio>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include "semaphore_control.h"

#define PROJECT_ID 65342

std::string Semaphore::get_path() {
    char buffer[1024];
    char *answer = getcwd(buffer, sizeof(buffer));
    std::string s_cwd;
    if (answer) {
        s_cwd = answer;
    }
    return s_cwd;
}

key_t gen_key(const std::string &executable_location) {
    return ftok(executable_location.c_str(), PROJECT_ID);
}

int get_semaphore(int (*creator)(key_t, int), int count) {
    std::string path = Semaphore::get_path();
    int key = gen_key(path);
    printf("key: %d\n", key);
    int semid = creator(key, count);
    return semid;
}

int Semaphore::init_semaphore(int count) {
    int semid = get_semaphore(create_semaphore, count);
    reset_semaphore(semid);
    return semid;
}

int Semaphore::get_shared_semaphore(int count) {
    return get_semaphore(get_created_semaphore, count);
}

int wrap_semget(key_t key, int count, short mode){
    int semafor = semget(key, count, mode);
    if (semafor == -1) {
        printf("Nie moglem utworzyc nowego semafora.\n");
        exit(EXIT_FAILURE);
    } else {
        printf("Semafor zostal utworzony : %d\n", semafor);
    }
    return semafor;
}

int Semaphore::create_semaphore(key_t sem_key, int how_many) {
    return wrap_semget(sem_key, how_many, 0777 | IPC_CREAT);
}

int Semaphore::get_created_semaphore(key_t sem_key, int how_many) {
   return wrap_semget(sem_key, how_many, 0777 | IPC_CREAT);
}

void Semaphore::reset_semaphore(int semid) {
    short values[2] = {1,0};
    int oepration_result = semctl(semid, 0, SETALL, &values);
    if (oepration_result == -1) {
        printf("Nie mozna ustawic semafora.\n");
        exit(EXIT_FAILURE);
    } else {
//        printf("Semafor zostal ustawiony.\n");
    }
}


void Semaphore::semaphore_await(int sem_cluster_id, unsigned short which) //semaphore_p
{
    int zmien_sem;
    sembuf bufor_sem;
    bufor_sem.sem_num = which;
    bufor_sem.sem_op = -1;
    bufor_sem.sem_flg = SEM_UNDO;
    zmien_sem = semop(sem_cluster_id, &bufor_sem, 1);
    if (zmien_sem == -1) {
        printf("Nie moglem zamknac semafora.\n");
        exit(EXIT_FAILURE);
    } else {
//        printf("Semafor S%d zostal zamkniety.\n", which);
    }
}

void Semaphore::semaphore_unlock(int semid, unsigned short which) //semaphore_v
{
    int zmien_sem;
    sembuf bufor_sem;
    bufor_sem.sem_num = which;
    bufor_sem.sem_op = 1;
    bufor_sem.sem_flg = SEM_UNDO;
    zmien_sem = semop(semid, &bufor_sem, 1);
    if (zmien_sem == -1) {
        printf("Nie moglem otworzyc semafora.\n");
        exit(EXIT_FAILURE);
    } else {
//        printf("Semafor S%d zostal otwarty.\n", which);
    }
}

void Semaphore::clear_sempahores(int semid) {
    int sem;
    sem = semctl(semid, 0, IPC_RMID);
    if (sem == -1) {
        printf("Nie mozna usunac semafora.\n");
        exit(EXIT_FAILURE);
    } else {
        printf("Semafor zostal usuniety : %d\n", sem);
    }
}