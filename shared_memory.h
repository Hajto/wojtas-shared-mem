//
// Created by Haito on 20.12.2017.
//

#ifndef WOJTAS_SHARED_MEMORY_SHARED_MEMORY_H
#define WOJTAS_SHARED_MEMORY_SHARED_MEMORY_H


#include <cstdlib>
#include <string>
#include <exception>
#include <functional>

namespace SharedMemoryCompanion {
    class SharedMemoryException : public std::exception {
        const char *what() const throw() override {
            return what_happened.c_str();
        }

    public:
        explicit SharedMemoryException(std::string happened) : what_happened(happened) {}

    private:
        std::string what_happened;
    };

}

class SharedMemory {
public:
    SharedMemory(std::string key_root, int bytes_of_mem);
    ~SharedMemory();

    void access_shared_memory(const std::function<void(void *)> &lend);

private:
    void* shared_address = NULL;
    int memid = 0;
    const int bytes_of_memory;

    int gen_id(int key);
    void obtain_address(int id);
};


#endif //WOJTAS_SHARED_MEMORY_SHARED_MEMORY_H
