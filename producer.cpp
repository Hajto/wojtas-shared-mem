#include <iostream>
#include "semaphore_control.h"
#include "shared_memory.h"
#include <fstream>
#include <string>

void produce(char c, int semid, SharedMemory *mem);

int main(int argc, char **args) {
    if (argc != 2)
        return 1;

    int semid = Semaphore::init_semaphore(2);
    Semaphore::reset_semaphore(semid);
    SharedMemory *memory = new SharedMemory("/", 1);

    std::ifstream input_file(args[1]);
    if (input_file.is_open()) {
        char buff = 2;
        while (input_file.get(buff)) {
            produce(buff, semid, memory);
        }
        produce(EOF, semid, memory);
        input_file.close();
    } else std::cout << "File failed to open" << std::endl;

    return 0;
}

void produce(char c, int semid, SharedMemory *mem) {
    const char constant = c;
    Semaphore::semaphore_await(semid, 0);
    mem->access_shared_memory([=](void *buffer) {
        *((char *) buffer) = constant;
    });
    Semaphore::semaphore_unlock(semid, 1);
}